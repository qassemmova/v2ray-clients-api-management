package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/xtls/xray-core/app/proxyman/command"
	"github.com/xtls/xray-core/common/protocol"
	"github.com/xtls/xray-core/common/serial"
	"github.com/xtls/xray-core/proxy/shadowsocks"
	"github.com/xtls/xray-core/proxy/trojan"
	"github.com/xtls/xray-core/proxy/vless"
	"google.golang.org/grpc"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
)

const (
	ApiAddress = "127.0.0.1"
	LEVEL      = 0
)

type AddedClient struct {
	// Define the fields you expect in the object
	UUID        string `json:"id"`
	Flow        string `json:"flow"`
	Method      string `json:"method"`
	InboundTag  string `json:"inbound_tag"`
	InboundType string `json:"inbound_type"`
}
type DeletedClient struct {
	// Define the fields you expect in the object
	Email      string `json:"email"`
	InboundTag string `json:"inbound_tag"`
}
type APIResponse struct {
	NewClients     []AddedClient   `json:"new_clients"`
	DeletedClients []DeletedClient `json:"deleted_clients"`
}

func fetchDataFromAPI(apiURL string) (APIResponse, error) {
	var response APIResponse
	resp, err := http.Get(apiURL)
	if err != nil {
		return response, err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Printf("%s", err)
		}
	}(resp.Body)
	if resp.StatusCode != http.StatusOK {
		return response, fmt.Errorf("HTTP request failed with status: %s", resp.Status)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return response, err
	}

	if err := json.Unmarshal(body, &response); err != nil {
		return response, err
	}

	return response, nil
}
func addVlessUser(c command.HandlerServiceClient, client AddedClient) error {
	var resp, err = c.AlterInbound(context.Background(), &command.AlterInboundRequest{
		Tag: client.InboundTag,
		Operation: serial.ToTypedMessage(&command.AddUserOperation{
			User: &protocol.User{
				Level: LEVEL,
				Email: client.UUID,
				Account: serial.ToTypedMessage(&vless.Account{
					Id:   client.UUID,
					Flow: client.Flow,
				}),
			},
		}),
	})
	if err != nil {
		log.Printf("failed to call grpc command: %v", err)
		return err
	} else {
		log.Printf("ok: %v", resp)
		return nil
	}
}
func addTrojanUser(c command.HandlerServiceClient, client AddedClient) error {
	resp, err := c.AlterInbound(context.Background(), &command.AlterInboundRequest{
		Tag: client.InboundTag,
		Operation: serial.ToTypedMessage(&command.AddUserOperation{
			User: &protocol.User{
				Level: LEVEL,
				Email: client.UUID,
				Account: serial.ToTypedMessage(&trojan.Account{
					Password: client.UUID,
					Flow:     client.Flow,
				}),
			},
		}),
	})

	if err != nil {
		log.Printf("failed to call grpc command: %v", err)
		return err
	} else {
		log.Printf("ok: %v", resp)
		return nil
	}
}
func addShadowsocsUser(c command.HandlerServiceClient, client AddedClient) error {
	resp, err := c.AlterInbound(context.Background(), &command.AlterInboundRequest{
		Tag: client.InboundTag,
		Operation: serial.ToTypedMessage(&command.AddUserOperation{
			User: &protocol.User{
				Level: LEVEL,
				Email: client.UUID,
				Account: serial.ToTypedMessage(&shadowsocks.Account{
					Password:   client.UUID,
					CipherType: shadowsocks.CipherType_CHACHA20_POLY1305,
				}),
			},
		}),
	})

	if err != nil {
		log.Printf("failed to call grpc command: %v", err)
		return err
	} else {
		log.Printf("ok: %v", resp)
		return nil
	}
}
func removeUser(c command.HandlerServiceClient, client DeletedClient) error {
	resp, err := c.AlterInbound(context.Background(), &command.AlterInboundRequest{
		Tag: client.InboundTag,
		Operation: serial.ToTypedMessage(&command.RemoveUserOperation{
			Email: client.Email,
		}),
	})
	if err != nil {
		log.Printf("failed to call grpc command: %v", err)
		return err
	} else {
		log.Printf("ok: %v", resp)
		return nil
	}
}
func main() {
	apiPort, _ := strconv.ParseInt(os.Args[1], 10, 64)
	fmt.Print(apiPort)
	apiLink := os.Args[2]
	fmt.Printf("apiLink is: %v \n apiPort is: %v\n", apiLink, apiPort)
	apiResponse, err := fetchDataFromAPI(apiLink)
	if err != nil {
		fmt.Println("Error fetching data from API:", err)
		return
	}

	cmdConn, err := grpc.Dial(fmt.Sprintf("%s:%d", ApiAddress, apiPort), grpc.WithInsecure())
	hsClient := command.NewHandlerServiceClient(cmdConn)
	if err != nil {
		fmt.Println("Error connecting to V2Ray:", err)
		os.Exit(1)
		return
	}
	for _, client := range apiResponse.NewClients {
		if client.InboundType == "vless" {
			fmt.Printf("adding client: %v, %v  \n", client.InboundTag, client.UUID)
			err := addVlessUser(hsClient, client)
			if err != nil {
				fmt.Println("Error adding VLESS user:", err)
			}
		} else if client.InboundType == "trojan" {
			fmt.Printf("adding client: %v, %v  \n", client.InboundTag, client.UUID)
			err := addTrojanUser(hsClient, client)
			if err != nil {
				fmt.Println("Error adding Trojan user:", err)
			}
		} else if client.InboundType == "shadowsocks" {
			fmt.Printf("adding client: %v, %v  \n", client.InboundTag, client.UUID)
			err := addShadowsocsUser(hsClient, client)
			if err != nil {
				fmt.Println("Error adding shadowsocks user:", err)
			}
		}
	}

	for _, client := range apiResponse.DeletedClients {
		fmt.Printf("remove client: %v, %v %v \n", client.InboundTag, client.Email)
		err := removeUser(hsClient, client)
		if err != nil {
			fmt.Println("Error removing user:", err)
		}
	}
	os.Exit(0)
}
